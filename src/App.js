import "./App.css";
import Main from "./Components/main";
import Country from "./Components/country";
import { BrowserRouter, Route, Switch } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
     

      <Switch>
        <Route exact path="/" component={Main} />               {/*if path is this (till localhost:3000) go to Main component*/}
        <Route exact path="/:handle" component={Country} />     {/*if path is this go to Country component*/}
      </Switch>
    </BrowserRouter>
  );
}

export default App;
