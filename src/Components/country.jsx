import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../App.css";

class Country extends Component {
  render() {
    console.log(this.props);
    let obj = this.props.location.hello.lovely.object;

    return (
      <div
        style={{
          display: "flex",
          flexWrap: "wrap",
          width: "100%",
          justifyContent: "space-around",
        }}
      >
        <div style={{ width: "100%", height: "10%", backgroundColor: "white" }}>
          <h1 style={{ marginLeft: "150px" }}>Where in the world</h1>
        </div>
        <div  className="m-5" style={{ height: "15%", width: "100%", backgroundColor: "white" }}>
          <Link to="/">
            <button
              style={{
                color: "black",
                padding: "10px 34px",
                textAlign: "center",
                margin: "2px",
                marginBottom:"4px",
                backgroundColor: "white",
                marginLeft: "150px",
                borderRadius: "5px",
                display: "flex",
                justifyContent: "space-evenly",
                width: "10vw",
              }}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                class="bi bi-arrow-left"
                viewBox="0 0 16 16"
              >
                <path
                  fill-rule="evenodd"
                  d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z"
                />
              </svg>
              Back
            </button>
          </Link>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            width: "50%",
            height: "90%",
            backgroundColor: "white",
          }}
        >
          <img
            className="country_image"
            style={{ alignSelf: "flex-start", height: "50%" }}
            src={this.props.location.hello.lovely.object.flags.png}
            alt=""
          />
        </div>
        <div
          style={{
            display: "flex",
            width: "50%",
            height: "90%",
            backgroundColor: "white",
          }}
        >
          <div>
            <h1>{obj.name.common}</h1>
            <div
              style={{
                display: "flex",
                flexDirection: "column",
                flexWrap: "wrap",
                width: "700px",
                height: "25vh",
                backgroundColor: "white",
                marginTop: "20px",
              }}
            >
              {/* <p>Native Name: {obj.name.nativeName.aym.official} </p> */}
              {
                <p>
                  Native Name: {Object.values(obj.name.nativeName)[0].official}{" "}
                </p>
              }
              <p>Population: &nbsp;{obj.population} </p>
              <p>Region: &nbsp;{obj.region}</p>
              <p>Sub Region: &nbsp;{obj.subregion} </p>
              <p>Capital:&nbsp; {obj.capital}</p>
              <p>Top Level Domain: &nbsp;{obj.tld}</p>
              {/* <p>Currencies: {obj.currencies.PEN.name}</p> */}
              {<p>Currencies:&nbsp; {Object.values(obj.currencies)[0].name}</p>}

              <p>
                Languages: &nbsp;
                {Object.values(obj.languages).map((lang) => {
                  return <span>{lang} &nbsp;</span>;
                })}{" "}
              </p>
            </div>

            {obj.borders ? (
              <div>
                {" "}
                Border Countries:
                {obj.borders.map((item) => {
                  return (
                    <span
                      style={{
                        padding: "8px 40px",
                        textAlign: "center",
                        marginLeft: "3%",
                        backgroundColor: "white",
                        border: "1px solid gray",
                      
                      }}
                    >
                      {item}
                    </span>
                  );
                })}
              </div>
            ) : null}
          </div>{" "}
        </div>
      </div>
    );
  }
}

export default Country;
