import React, { Component } from "react";
import "../App.css";

class Input extends Component {
  constructor(props) {
    super(props);

    this.state = { inputText: "" };
  }

  handleChange = (e) => {
    this.setState({ inputText: e.target.value }); //handleChange wali value local state par save hogi
  };

 

 
  render() {
    return (
      <form >
       
        <input
          type="text"
          placeholder="Search for a country..."
          value={this.state.inputText}
          onChange={this.handleChange}
          className="input_box"
        />
      </form>
    );
  }
}

export default Input;