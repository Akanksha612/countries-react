import React, { Component } from "react";
import { Link } from "react-router-dom";
import "../App.css";

//Main can't be Country's parent -first of all
//It's not rendering Country Component

class Main extends Component {
  state = { data: [], Loaded: false, input: "" };

  //fetch should be put inside componentDidMount()

  componentDidMount() {
    fetch("https://restcountries.com/v3.1/all")
      .then((response) => response.json())
      .then((data) => {
        this.setState({ data: data, Loaded: true });
        console.log(data);
      });
  }

  handleRegion = (e) => {
    fetch(`https://restcountries.com/v3.1/region/${e.target.value}`)
      .then((response) => response.json())
      .then((data) => this.setState({ data: data, Loaded: true }));
  };

  handleChange = (e) => {
    this.setState({ input: e.target.value });
    //console.log(this.state)
  };

  displayCountries = () => {
    const arr = this.state.data.filter((object) =>
      object.name.common.toLowerCase().includes(this.state.input.toLowerCase())
    );

    //this.setState({data: arr});

    if (arr) {
      return arr;
    } else {
      return [];
    }
  };

  render() {
    console.log(this.state.data);
    let display = this.displayCountries();

    return (
      <div>
        <div className="header">
          <h1 style={{ marginLeft: "200px" }}>Where in the world?</h1>
        </div>

        <div
          style={{
            display: "flex",
            justifyContent: "space-evenly",
            marginTop: "30px",
            marginBottom: "30px",
            width: "80%",
          }}
        >
          {" "}
          {/*  div for search and filter */}
          <input
            className="input"
            placeholder="Search for a country..."
            type="text"
            onChange={this.handleChange}
          />
          <select
            className="filter"
            onChange={this.handleRegion}
            placeholder="Filter by Region"
            name="regions"
            id="regions"
          >
            <option value="none" selected disabled hidden>
              Filter by Region
            </option>
            <option value="africa">Africa</option>
            <option value="america">America</option>
            <option value="asia">Asia</option>
            <option value="europe">Europe</option>
            <option value="oceania">Oceania</option>
          </select>
        </div>

        <div
          style={{
            display: "flex",
            flexWrap: "wrap",
            justifyContent: "centre",
            alignItems: "center",
            width: "80vw",
            margin: "auto",
          }}
        >
          {this.state.Loaded
            ? display.map((object) => {
                return (
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "column",
                      padding: "20px",
                      margin: "10px",
                      boxShadow: "rgba(100, 100, 111, 0.2) 0px 7px 29px 0px",
                    }}
                  >
                    <img src={object.flags.png} alt="" />

                    <Link
                      to={{
                        pathname: `/${object.name.common.toLowerCase()}`,
                        hello: { lovely: { object } },
                      }}
                    >
                      <h3>{object.name.common}</h3>
                    </Link>
                    <p>Population : {object.population}</p>
                    <p>Region: {object.region}</p>
                    <p>Capital: {object.capital}</p>
                  </div>
                );
              })
            : "Loading"}
        </div>
      </div>
    ); //return
  }
}

export default Main;
